﻿using UnityEngine;
using System.Collections;

public class AnimationStart : MonoBehaviour {

	public Animator animator;

	void Start(){
		animator.enabled = false;
	}

	void OnTriggerEnter () {
		animator.enabled = true;
	}
}
