﻿using UnityEngine;
using System.Collections;

public class AnimationStop : MonoBehaviour {

	public Animator animator;

	void Start(){
		animator.enabled = false;
	}

	void OnTriggerEnter () {
		animator.enabled = false;
	}

}
