﻿using UnityEngine;
using System.Collections;

public class DayNightCycle : MonoBehaviour
{

	public float minutesInDay = 1.0f;

	const float LIGHT_CHANGE = 0.05f;
	const float MAX_LIGHT = 1.0f;
	const float MIN_LIGHT = 0.0f;
	float timer;
	float percentageOfDay;
	float turnSpeed;

	void Start ()
	{
		timer = 0.0f;
	}

	void Update ()
	{
		updatePercentageOfDay ();
		updateLights ();
		updateTurnSpeed ();
		rotateLights ();
	}

	void updatePercentageOfDay ()
	{
		timer += Time.deltaTime;
		percentageOfDay = timer / secondsInDay ();

		if (timer > secondsInDay ())
			timer = 0.0f;
	}

	void updateLights ()
	{
		Light light = GetComponent<Light> ();
		if (isNight ()) {
			if (light.intensity > MIN_LIGHT)
				light.intensity -= LIGHT_CHANGE;
		} else {
			if (light.intensity < MAX_LIGHT)
				light.intensity += LIGHT_CHANGE;
		}
	}

	void updateTurnSpeed ()
	{
		turnSpeed = 360.0f / secondsInDay () * Time.deltaTime;
	}

	void rotateLights ()
	{
		//transform.RotateAround (transform.position, transform.right, turnSpeed);
		transform.RotateAround(new Vector3(0.0f, 0.0f, 0.0f), transform.right, turnSpeed);
	}

	float secondsInDay ()
	{
		return minutesInDay * 60.0f;
	}

	bool isNight ()
	{
		return percentageOfDay > 0.5f;
	}
}
